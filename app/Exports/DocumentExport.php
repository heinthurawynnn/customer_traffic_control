<?php

namespace App\Exports;

use App\Document;
use App\Models\Document as ModelsDocument;
use League\CommonMark\Block\Element\Document as ElementDocument;
use Maatwebsite\Excel\Concerns\FromCollection;

class DocumentExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ModelsDocument::all();
    }
}
