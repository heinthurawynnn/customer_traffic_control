<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'document_no',
        'pcode_id',
        'status',
        'prepared_by',
        'prepared_at',
        'checked_at',
        'checked_by',
        'approved_by',
        'approved_at',
        'exported_by',
        'exported_at',
        'deleted_at',
    ];

    public function prepared(){
        return $this->belongsTo(User::class,'prepared_by');
    }
    public function checked(){
        return $this->belongsTo(User::class,'checked_by');
    }
    public function approved(){
        return $this->belongsTo(User::class,'approved_by');
    }
    public function exported(){
     return $this->belongsTo(User::class,'exported_by');
    }
}
