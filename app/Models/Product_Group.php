<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Group extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'product_groups';
    protected $fillable = [
        'product_category_id', 'product_group_id', 'product_group_code', 'product_group_name'
    ];
}
