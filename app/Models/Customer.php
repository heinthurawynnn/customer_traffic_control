<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'branch_id',
        'time',
        'date_client',
        'customers',
        'vehicles',
        'tot_cus',
        'tot_veh',
        'created_by',
        'updated_by',

    ];
    // protected $casts = [
    //     'customers' => 'array',
    //     'vehicles' => 'array',
    // ];
    public function branches()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function fields()
    {
        return $this->belongsTo(Field::class, 'name');
    }

    public function periods()
    {
        return $this->belongsTo(Period::class, 'time');
    }
    public function user_name()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
