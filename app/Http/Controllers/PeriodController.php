<?php

namespace App\Http\Controllers;

use App\Models\Period;
use Illuminate\Http\Request;

class PeriodController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:period-create', ['only' => ['index', 'show']]);
        $this->middleware('permission:period-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:period-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:period-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods  = Period::latest()->paginate(15);
        return view('periods.index', compact('periods'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periods = Period::all();
        return view('periods.create', compact('periods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'time1' => 'required',
            'time2' => 'required',
        ]);
        $time1 = date('h:i A', strtotime($request['time1']));
        // $time1= $time1->format('h:i a') ;
        //  $time2=trim($request['time2']);
        // $time2= $time2->format('h:i a') ;
        $time2 = date('h:i A', strtotime($request['time2']));

        $request['time'] = $time1 . '-' . $time2;
        // dd(  $request['time1'] );
        Period::create($request->all());
        return redirect()->route('periods.index')
            ->with('success', 'Periods record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function show(Period $period)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function edit(Period $period)
    {
        return view('periods.edit', compact('period'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Period $period)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function destroy(Period $period)
    {
        $period->delete();

        return redirect()->route('periods.index')
            ->with('success', 'Time deleted successfully');
    }
}
