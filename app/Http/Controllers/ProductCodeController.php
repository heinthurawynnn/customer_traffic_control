<?php
namespace App\Http\Controllers;
use App\Models\ProductCode;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product_Category;
use App\Models\Product_Design;
use App\Models\Product_Group;
use App\Models\Product_Pattern;
use App\Models\Supplier;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductCodeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:pcode-list|pcode-create|pcode-edit|pcode-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:pcode-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pcode-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pcode-delete', ['only' => ['destroy']]);
    }
    protected function pg_con()
    {
        $pg_con =  pg_connect("host=192.168.2.241 port=5432 dbname=compare user=postgres password=p@ssw0rd options='--client_encoding=UTF8'")
            or die('Could not connect: ' . pg_last_error());
        return $pg_con;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductCode::latest()->paginate(30);

        return view('products.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 30);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $u_id = Auth::id();
        $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        $u_cat = $u_obj->mercate_id;
        if ($u_cat) {
            $category = Product_Category::where('id', $u_cat)->first();
            $categories = null;
        } else {
            $categories = Product_Category::query()->get();
            $category = null;
        }
        $vendors = Supplier::query()->get();
        $brands = Brand::query()->get();
        $units = Unit::query()->get();
        return view('products.create', compact('categories', 'category', 'brands', 'vendors', 'units'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'type' => 'required',
            'product_code_no' => 'nullable|min:9|max:20',
            'product_name' => 'required',
            'category_id' => 'required|numeric|min:0|not_in:0',
            'group_id' => 'required|numeric|min:0|not_in:0',
            'pattern_id' => 'required|numeric|min:0|not_in:0',
            'design_id' => 'required|numeric|min:0|not_in:0',
            'unit_id' => 'required|numeric|min:0|not_in:0',
            'brand_id' => 'required|numeric|min:0|not_in:0',
            'supplier_id' => 'required|numeric|min:0|not_in:0',
            'product_pack_flag' => 'required',
            'doc_no'
        ]);
        $request['created_by']  = Auth::id();
        ProductCode::create($request->all());
        return redirect()->route('product_Codes.index')
            ->with('success', 'Product created successfully.');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCode  $productCode
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCode $product_Code)
    {
        $designs = Product_Design::query()->get();
        $patterns  = Product_Pattern::query()->get();
        $groups  = Product_Group::query()->get();
        $suppliers = Supplier::query()->get();
        $brands = Brand::query()->get();
        $units = Unit::query()->get();
        $categories = Product_Category::all();
        $products = ProductCode::latest()->paginate(1);
        return view('products.show', compact(
            'product_Code',
            'categories',
            'suppliers',
            'brands',
            'units',
            'groups',
            'patterns',
            'designs',
            'products'
        ));
        // return view('products.show', compact('product_Code'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCode  $productCode
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCode $product_Code)
    {
        $u_id = Auth::id();
        $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        $u_cat = $u_obj->mercate_id;
        if ($u_cat) {
            $category = Product_Category::where('id', $u_cat)->first();
            $categories = null;
        } else {
            $categories = Product_Category::query()->get();
            $category = null;
        }
        $designs = Product_Design::query()->get();
        $patterns  = Product_Pattern::query()->get();
        $groups  = Product_Group::query()->get();
        $suppliers = Supplier::query()->get();
        $brands = Brand::query()->get();
        $units = Unit::query()->get();
        $vendors = Supplier::query()->get();
        $categories = Product_Category::all();
        return view('products.edit', compact(
            'product_Code',
            'categories',
            'category',
            'suppliers',
            'brands',
            'units',
            'groups',
            'patterns',
            'designs',
            'vendors',
        ));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCode  $productCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCode $product_Code)
    {
        request()->validate([
            'type' => 'required',
            'product_code_no' => 'nullable|min:9|max:20',
            'product_name' => 'required',
            'category_id' => 'required|numeric|min:0|not_in:0',
            'group_id' => 'required|numeric|min:0|not_in:0',
            'pattern_id' => 'required|numeric|min:0|not_in:0',
            'design_id' => 'required|numeric|min:0|not_in:0',
            'unit_id' => 'required|numeric|min:0|not_in:0',
            'brand_id' => 'required|numeric|min:0|not_in:0',
            'supplier_id' => 'required|numeric|min:0|not_in:0',
            'product_pack_flag' => 'required',
        ]);
        $update['type'] = $request->get('type');
        $update['product_code_no'] = $request->get('product_code_no');
        $update['product_name'] = $request->get('product_name');
        $update['category_id'] = $request->get('category_id');
        $update['group_id'] = $request->get('group_id');
        $update['pattern_id'] = $request->get('pattern_id');
        $update['design_id'] = $request->get('design_id');
        $update['unit_id'] = $request->get('unit_id');
        $update['brand_id'] = $request->get('brand_id');
        $update['supplier_id'] = $request->get('supplier_id');
        $update['product_pack_flag'] = $request->get('product_pack_flag');
        $update['created_by']  = Auth::id();
        $product_Code->update($request->all());
        return redirect()->route('product_Codes.index')
            ->with('success', 'Product Code updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCode  $productCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCode $product_Code)
    {
        $pg_con = $this->pg_con();
        if($pg_con){
            $product_code_no = $product_Code->product_code_no;
            $update_sql = "UPDATE new_productcode_generate.generatecode   
            SET insert_status = null, reserve_date = null
            WHERE insert_status = 'G' AND new_code =  '" . $product_code_no . "' AND reserve_date is not null";
            $result = pg_query($update_sql);
            if ($result) {
                $product_Code->delete();
                return redirect()->route('product_Codes.index')
                    ->with('success', 'Product Code deleted successfully');
                }
            }
    }
    /////DropdownAjax//////
    public function get_groups($cat_id)
    {
        $data = Product_Group::where('product_category_id', $cat_id)->orderBy('product_group_name', 'ASC')->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }
    public function get_patterns($gp_id)
    {
        $data = Product_Pattern::where('product_group_id', $gp_id)->orderBy('product_pattern_name', 'ASC')->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }
    public function get_designs($pat_id)
    {
        $data = Product_Design::where('product_pattern_id', $pat_id)->orderBy('product_design_name','ASC')->get();
        // dd($data);
        Log::info($data);
        return response()->json(['data' => $data]);
    }
    public function get_category_by_id($id)
    {
        $category_name = Product_Category::select('product_category_name')->where('id', $id)->first();
        return  response()->json(['data' => $category_name->product_category_name]);
    }
    //    get_pcode_gen
    public function get_pcode_gen()
    {
        $pg_con = $this->pg_con();
        if ($pg_con) {
            $deleted_unused_generated_code = "UPDATE new_productcode_generate.generatecode   
            SET insert_status = null, reserve_date = null
            WHERE insert_status = 'G' AND reserve_date < current_date - interval '6 days'";
            pg_query($deleted_unused_generated_code);

            $sql = "SELECT new_code FROM new_productcode_generate.generatecode WHERE insert_status is NULL AND barcode_code is NULL LIMIT 1";
            $result = pg_query($sql);
            $code  = pg_fetch_result($result, 0, 'new_code');
            $update_sql = "UPDATE new_productcode_generate.generatecode   
            SET insert_status = 'G', reserve_date = now()
            WHERE new_code =  '" . $code . "'";
            $result = pg_query($update_sql);
            if ($result) {
                return  response()->json(['data' => $code], 200);
            } else {
                return  response()->json(NULL, 500);
            }
        }
    }
}
