<?php

namespace App\Http\Controllers;

use App\Models\Product_Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-group-list|product-group-create|product-group-edit|product-group-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:product-group-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-group-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-group-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $product_Groups = Product_Group::orderBy('product_group_id', 'ASC')->paginate(15);
        return view('product_Groups.index', compact('product_Groups'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_Groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'product_group_id' => 'required',
            'product_group_code' => 'required',
            'product_group_name' => 'required',

        ]);

      
        $request['created_by']  = Auth::id();
        Product_Group::create($request->all());

        return redirect()->route('product_Groups.index')
            ->with('success', 'Product_Group created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_Group  $product_Group
     * @return \Illuminate\Http\Response
     */
    public function show(Product_Group $product_Group)
    {
        return view('product_Groups.show', compact('product_Group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_Group  $product_Group
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_Group $product_Group)
    {
        return view('product_Groups.edit', compact('product_Group'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_Group  $product_Group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_Group $product_Group)
    {
        request()->validate([
            'product_group_id' => 'required',
            'product_group_code' => 'required',
            'product_group_name' => 'required',

        ]);
        $update = [];
        if ($files = $request->file('img')) {
            $destinationPath = 'public/image/'; // upload path
            // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $org_file_name = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $filename = $org_file_name . $extension;
            $files->move($destinationPath, $filename);
            $update['img'] = "$org_file_name";
        }


        $update['product_group_id'] = $request->get('product_group_id');
        $update['product_group_code'] = $request->get('product_group_code');
        $update['product_group_name'] = $request->get('product_group_name');
        $update['created_by']  = Auth::id();
        $product_Group->update($request->all());

        return redirect()->route('product_Groups.index')
            ->with('success', 'Product_Group updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_Group  $product_Group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_Group $product_Group)
    {
        $product_Group->delete();

        return redirect()->route('product_Groups.index')
            ->with('success', 'Product_Group deleted successfully');
    }
}
