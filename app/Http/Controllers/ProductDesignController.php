<?php

namespace App\Http\Controllers;

use App\Models\Product_Design;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductDesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-design-list|product-design-create|product-design-edit|product-design-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:product-design-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-design-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-design-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $product_Designs = Product_Design::orderBy('product_design_id', 'ASC')->paginate(15);
        return view('product_Designs.index', compact('product_Designs'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_Designs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'product_design_id' => 'required',
            'product_design_code' => 'required',
            'product_design_name' => 'required',

        ]);


        $request['created_by']  = Auth::id();
        Product_Design::create($request->all());

        return redirect()->route('product_Designs.index')
            ->with('success', 'Product Design created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product_Design  $product_Design
     * @return \Illuminate\Http\Response
     */
    public function show(Product_Design $product_Design)
    {
        return view('product_Designs.show', compact('product_Design'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product_Design  $product_Design
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_Design $product_Design)
    {
        return view('product_Designs.edit', compact('product_Design'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product_Design  $product_Design
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_Design $product_Design)
    {
        request()->validate([
            'product_design_id' => 'required',
            'product_design_code' => 'required',
            'product_design_name' => 'required',

        ]);
        
        $update['product_design_id'] = $request->get('product_design_id');
        $update['product_design_code'] = $request->get('product_design_code');
        $update['product_design_name'] = $request->get('product_design_name');
        $update['created_by']  = Auth::id();
        $product_Design->update($request->all());

        return redirect()->route('product_Designs.index')
            ->with('success', 'Product_Design updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product_Design  $product_Design
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_Design $product_Design)
    {
        $product_Design->delete();

        return redirect()->route('product_Designs.index')
            ->with('success', 'Product Design deleted successfully');
    }
}
