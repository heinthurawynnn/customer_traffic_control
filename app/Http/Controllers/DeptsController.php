<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Depts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeptsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:dept-list|dept-create|dept-edit|dept-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:dept-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:dept-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:dept-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $depts = Depts::latest()->paginate(5);
        return view('depts.index', compact('depts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    // {pluck('name','name')->
    {
        $branches = Branch::all();
        return view('depts.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'name' => 'required',
            'branch_id' => 'required'
        ]);

        Depts::create($request->all());

        return redirect()->route('depts.index')
            ->with('success', 'Department created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dept  $dept
     * @return \Illuminate\Http\Response
     */
    public function show(Depts $dept)
    {
        return view('depts.show', compact('dept'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Depts  $dept
     * @return \Illuminate\Http\Response
     */
    public function edit(Depts $dept)
    {
        $branches = Branch::all();
        return view('depts.edit', compact('dept', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Depts  $dept
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Depts $dept)
    {
        request()->validate([
            'name' => 'required',

        ]);
        $update = [];
        if ($files = $request->file('img')) {
            $destinationPath = 'public/image/'; // upload path
            // $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $org_file_name = $files->getClientOriginalName();
            $extension = $files->getClientOriginalExtension();
            $filename = $org_file_name . $extension;
            $files->move($destinationPath, $filename);
            $update['img'] = "$org_file_name";
        }


        $update['name'] = $request->get('name');
        $update['created_by']  = Auth::id();

        $dept->update($request->all());

        return redirect()->route('depts.index')
            ->with('success', 'Department updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Depts  $dept
     * @return \Illuminate\Http\Response
     */
    public function destroy(Depts $dept)
    {
        $dept->delete();

        return redirect()->route('depts.index')
            ->with('success', 'Department deleted successfully');
    }
}
