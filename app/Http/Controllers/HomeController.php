<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\Document;
use App\Models\Mercat;
use App\Models\Product_Category;
use App\Models\Product_Design;
use App\Models\Product_Group;
use App\Models\Product_Pattern;
use App\Models\ProductCode;
use App\Models\Supplier;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Services\DataTable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:prepare-merchandise', ['only' => ['generate_doc_no', 'prepare', 'prepare_update', 'doc_prepare_save', 'document_list', 'pcode_store']]);
        $this->middleware('permission:approve-manager|checker-checked', ['only' => 'approve_view']);
        $this->middleware('permission:approve-manager', ['only' => ['approve', 'doc_approve_save']]);
        $this->middleware('permission:checker-checked', ['only' => ['checked', 'doc_checked_save']]);
        $this->middleware('permission:reject-manager', ['only' => ['doc_destory']]);
        $this->middleware('permission:pcode-delete', ['only' => ['pcode_destory']]);
        $this->middleware('role:admin', ['only' => ['admin']]);
    }
    protected function pg_con()
    {
        $pg_con =  pg_connect("host=192.168.2.241 port=5432 dbname=compare user=postgres password=p@ssw0rd options='--client_encoding=UTF8'")
            or die('Could not connect: ' . pg_last_error());
        return $pg_con;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        } else {
            return view('employee.login');
        }
    }
    public function home()
    {
        return view('home');
    }
    public function welcome()
    {
        return view('welcome');
    }
    public function create()
    {
        return view('new_code_create');
    }
    protected function generate_doc_no()
    {
        $doc = Document::withTrashed()->latest()->first(); //doc obj of last inserted
        $doc == null ? $last_doc_no = "NCR_" . date('d_m_Y') . "-0001" : $last_doc_no = $doc->document_no;
        $doc_number_arr = explode("-", $last_doc_no); // explode document no only
        $doc_no_preface_arr = explode("_", $doc_number_arr[0]); //explode document preface 
        $doc_dmy = $doc_no_preface_arr[1] . "_" . $doc_no_preface_arr[2] . "_" . $doc_no_preface_arr[3]; // get the d m y format like date('d_m_Y')
        if ($doc_dmy == date('d_m_Y')) {  // add 1 when the doc_no is in same day 
            $doc_no = $doc_number_arr[1]; // document no only
            $doc_no = str_pad($doc_no + 1, 4, 0, STR_PAD_LEFT);
        } else { // reset doc_no when the next day 
            $doc_no = "0001";
        }
        $doc_no_format = "NCR_" . date('d_m_Y') . "-" . $doc_no;
        return $doc_no_format;
    }
    public function prepare()
    {
        $document = new Document();
        $document_no = $this->generate_doc_no();
        $document->document_no = $document_no;
        $document->prepared_by = Auth::id();
        $document->deleted_at = now();
        $document->save();
        $doc_id = $document->id;
        $url = route('prepare_update', $doc_id);
        return redirect($url);
    }

    public function prepare_update($id, Document $document)
    {

        $doc_id = $id;
        $document_no_result = Document::select('document_no')->where('id', $doc_id)->withTrashed()->first();
        $document_no_result == null ? $document_no = null : $document_no = $document_no_result->document_no;
        $u_id = Auth::id();
        $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        //1,2,3

        $u_cat = $u_obj->mercate_id;
        if ($u_cat) {
            $u_cat_arr =  explode(',', $u_cat);
            $category  = Product_Category::whereIn('product_category_id', $u_cat_arr)->get();
            // $category = Product_Category::where('product_category_id', '1')->first();
            // dd($category);
            $categories = null;
        } else {
            $categories = Product_Category::query()->get();
            $category = null;
        }
        $vendors = Supplier::query()->orderBy('vendor_name', 'ASC')->get();
        $brands = Brand::query()->orderBy('product_brand_name', 'ASC')->get();
        $units = Unit::query()->orderBy('product_unit_name', 'ASC')->get();
        $products = ProductCode::where('doc_no', $doc_id)->orderByDesc('id')->paginate(10);
        return view('new_code_prepare', compact('doc_id', 'products', 'document', 'categories', 'document_no', 'category', 'brands', 'vendors', 'units'))->with('i', (request()->input('page', 1) - 1) * 10);
    }
    public function doc_prepare_save(Request $request, $id)
    {
        $input = $request->all();

        try {
            $document = Document::withTrashed()->findOrFail($id);
            $request['prepared_at'] = now();
            $request['deleted_at'] = null;
            $request['status'] = 0;
            $input = $request->all();
            $input['pcode_id'] = trim($input['pcode_id'], ",");
            if (empty($input['pcode_id'])) {
                return back()->withErrors(['document_no' => 'There was no created code was selected']);
            }
            // dd($input);
            $document->fill($input)->save();
            $uid = Auth::id();
            return redirect()->route('documents_list', ['id' => $uid])
                ->with('success', 'Document has successfully requested .');
        } catch (ModelNotFoundException $err) {
            // redirect()->route('prepare')
            return back()->withErrors(['document_no' => 'Document No not genereated please try to request new document']);
        }
    }
    public function documents_list(Request $request)
    {
        $empty_docs = Document::onlyTrashed()->whereNull('pcode_id')->where('prepared_by', Auth::id())->forceDelete();
        return view('document_list');
    }
    public function document_list(Request $request)
    {
        if ($request->ajax()) {
            // $documents = Document::latest()->get();
            $documents = Document::where('prepared_by', Auth::id())->latest()->get();
            $data = array();
            foreach ($documents as $doc) {
                $codes = explode(",", $doc->pcode_id);
                $codes_count = count($codes);
                // $doc_id = '/document_view/' . $doc->id;
                if ($doc->status == 0) {
                    $doc_status = 'Requested';
                } else if ($doc->status == 1) {
                    $doc_status = 'Approved';
                } else if ($doc->status == 3) {
                    $doc_status = 'Checked';
                } else {
                    $doc_status = 'Finished';
                }
                $user = User::where('id', Auth::id())->first();
                $data_row_arr = array(
                    'id' => $doc->id,
                    'status' => $doc_status,
                    // 'category_name' => $category_name,
                    'document_no' => $doc->document_no,
                    'pcode_id'    => $codes_count,
                    'prepared_by' => $doc->prepared->name,
                    'prepared_at' => $doc->prepared_at,
                    'checked_by' => $doc->checked_by ? $doc->checked->name : "",
                    'checked_at' => $doc->checked_at,
                    'approved_by' => $doc->approved_by ? $doc->approved->name : "",
                    'approved_at' => $doc->approved_at,
                    'exported_by' => $doc->exported_by ? $doc->exported->name : "",
                    'exported_at' => $doc->exported_at
                );
                $data[] = (object) $data_row_arr;
            }
            return DataTables()::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    // {{ route("approve_view", $document->id) }}
                    if ($data->status == 'Requested' || $data->status == 'Checked') {
                        $btn = '<a href="/document_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    } else {
                        $btn = '<a href="/document_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);

            // return DataTables()::of($data)
            //     ->addIndexColumn()
            //     ->addColumn('action', function ($data) {
            //         // {{ route("approve_view", $document->id) }}
            //         $btn = '<a href="' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
            //         return $btn;
            //     })
            //     ->rawColumns(['action'])
            //     ->make(true);
        }
        return view('document_list');
    }
    public function pcode_store(Request $request)
    {
        if ($request['product_code_no'] == null && $request['generate_product_code_no'] != null) {
            $request['product_code_no'] = $request['generate_product_code_no'];
        }

        request()->validate([
            'type' => 'required',
            'product_code_no' => 'required|min:9|max:20',
            'product_name' => 'required',
            'category_id' => 'required|numeric|min:0|not_in:0',
            'group_id' => 'required|numeric|min:0|not_in:0',
            'pattern_id' => 'required|numeric|min:0|not_in:0',
            'design_id' => 'required|numeric|min:0|not_in:0',
            'unit_id' => 'required|numeric|min:0|not_in:0',
            'brand_id' => 'required|numeric|min:0|not_in:0',
            'supplier_id' => 'required|numeric|min:0|not_in:0',
            'product_pack_flag' => 'required',
            'doc_no' => 'required'

        ]);
        $request['created_by'] = Auth::id();
        ProductCode::create($request->all());
        return back();
    }
    public function manager_product_codes_edit($id, ProductCode $product_Code)
    {
        $product = ProductCode::find($id);
        // $u_id = Auth::id();
        // $u_obj = User::select('mercate_id')->where('id', $u_id)->first();
        // $u_cat = $u_obj->mercate_id;
        // if ($u_cat) {
        //     $category = Product_Category::where('id', $u_cat)->first();
        //     $categories = null;
        // } else {
        //     $categories = Product_Category::query()->get();
        //     $category = null;
        // }
        $designs = Product_Design::query()->get();
        $patterns  = Product_Pattern::query()->get();
        $groups  = Product_Group::query()->get();
        $suppliers = Supplier::query()->get();
        $brands = Brand::query()->get();
        $units = Unit::query()->get();
        $vendors = Supplier::query()->get();
        $categories = Product_Category::all();
        // 'categories',
        // 'category',
        return view('manager_product_codes_edit', compact(
            'product',
            'product_Code',
            'suppliers',
            'brands',
            'units',
            'groups',
            'patterns',
            'designs',
            'vendors',
        ));
    }
    public function manager_product_codes_update(Request $request)
    {
        request()->validate([
            'product_id' => 'required',
            'product_name' => 'required',
        ]);
        $pid =  $request->get('product_id');
        $product_Code = ProductCode::find($pid);
        $product_Code->update($request->all());
        // $product_Code->fill($input)->save();

        $doc = Document::where('pcode_id', 'like', "%{$pid}%")->first();
        $doc_id = $doc->id;
        return redirect()->route('approve_view', $doc_id)
            ->with('success', 'Product Code updated successfully');
    }
    protected function generate_code_delete($product_code_no)
    {
        $pg_con = $this->pg_con();
        if ($pg_con) {

            $update_sql = "UPDATE new_productcode_generate.generatecode   
            SET insert_status = null, reserve_date = null
            WHERE insert_status = 'G' AND new_code  = '$product_code_no' AND reserve_date is not null";
            $result = pg_query($update_sql);
        }
        return $result;
    }

    public function pcode_destory($id)
    {
        $product_Code = ProductCode::find($id);
        if ($product_Code) {
            $product_code_no = $product_Code->product_code_no;
            $gen_code_delete = $this->generate_code_delete($product_code_no);
            if ($gen_code_delete) {
                $product_Code->forceDelete();
                return back()->with('success', 'Product Code deleted successfully');
            }
        }
    }
    public function admin()
    {
        return view('admin');
    }

    public function approve()
    {
        $approved = Document::where('status', '=', 1)->whereNotNull('approved_at')->whereNull('exported_by')->orderBy('approved_at', 'asc')->get();
        $done_docs = Document::where('status', '=', 2)->whereNotNull('exported_by')->orderBy('exported_at', 'asc')->get();
        return view('document_list_for_manager', compact('approved', 'done_docs'));
    }
    public function export()
    {
        $approved = Document::where('status', '=', 1)->whereNotNull('approved_at')->whereNull('exported_by')->orderBy('approved_at', 'asc')->get();
        $done_docs = Document::where('status', '=', 2)->whereNotNull('exported_by')->orderBy('exported_at', 'asc')->get();
        return view('document_list_for_SD', compact('approved', 'done_docs'));
    }
    public function checker()
    {
        return view('document_list_for_checker');
    }

    public function document_view($doc_id)
    {
        $document = Document::find($doc_id); //doc obj of last inserted
        $products = ProductCode::where('doc_no', $doc_id)->orderByDesc('id')->paginate(10);
        return view('document_view', compact('products', 'document'))->with('i', (request()->input('page', 1) - 1) * 10);
    }
    public function approve_view($doc_id)
    {
        $document = Document::find($doc_id); //doc obj of last inserted
        $products = ProductCode::where('doc_no', $doc_id)->orderByDesc('id')->paginate(10);
        return view('approve_view', compact('products', 'document',))->with('i', (request()->input('page', 1) - 1) * 10);
    }
    public function doc_checked_save(Request $request, $id)
    {
        try {
            $document = Document::withTrashed()->findOrFail($id);
            $request['checked_at'] = now();
            $request['deleted_at'] = null;
            $request['status'] = 3;
            $input = $request->all();
            $input['pcode_id'] = trim($input['pcode_id'], ",");
            if (empty($input['pcode_id'])) {
                return back()->withErrors(['document_no' => 'There was no created code was selected']);
            }
            // dd($input);
            $document->fill($input)->save();
            $uid = Auth::id();
            return redirect()->route('checker', ['id' => $uid])
                ->with('success', 'Document has been Checked .');
        } catch (ModelNotFoundException $err) {
            // redirect()->route('prepare')
            return back()->withErrors(['document_no' => 'Document No not genereated please try to request new document']);
        }
    }
    public function doc_approve_save(Request $request, $id)
    {
        try {
            $document = Document::withTrashed()->findOrFail($id);
            $request['approved_at'] = now();
            $request['deleted_at'] = null;
            $request['status'] = 1;
            $input = $request->all();
            $input['pcode_id'] = trim($input['pcode_id'], ",");
            if (empty($input['pcode_id'])) {
                return back()->withErrors(['document_no' => 'There was no created code was selected']);
            }
            // dd($input);
            $document->fill($input)->save();
            $uid = Auth::id();
            return redirect()->route('approve', ['id' => $uid])
                ->with('success', 'Document has successfully approved .');
        } catch (ModelNotFoundException $err) {
            // redirect()->route('prepare')
            return back()->withErrors(['document_no' => 'Document No not genereated please try to request new document']);
        }
    }
    public function doc_export($id)
    {
        $document = Document::find($id);
        $pcode_id = $document->pcode_id;
        // dd($pcode_id);
        if ($pcode_id) {
            $pcode_arr = explode(',', $pcode_id);
            foreach ($pcode_arr as $p_id) {
                $this->pcode_find($p_id);
            }
        }
        $input['status'] = 2;
        $input['exported_at'] = now();
        $input['exported_by'] = Auth::id();
        $document->fill($input)->save();

        return redirect()->route('approve')
            ->with('success', 'Document save into Database');
    }
    public function pcode_find($id)
    {
        $product_Code = ProductCode::find($id);
        if ($product_Code) {
            $product_code_no = $product_Code->product_code_no;
            $generate_code_update = $this->generate_code_update($product_code_no);
        }
    }
    protected function generate_code_update($product_code_no)
    {
        $pg_con = $this->pg_con();
        if ($pg_con) {
            $now = now();
            $update_sql = "UPDATE new_productcode_generate.generatecode   
            SET insert_status = 'T', reserve_date = '$now'
            WHERE insert_status = 'G' AND new_code  = '$product_code_no' AND reserve_date is not null";
            $result = pg_query($update_sql);
        }
        return $result;
    }

    public function doc_destory($id)
    {
        $document = Document::find($id);
        $pcode_id = $document->pcode_id;
        // dd($pcode_id);
        if ($pcode_id) {
            $pcode_arr = explode(',', $pcode_id);
            foreach ($pcode_arr as $p_id) {
                $this->pcode_destory($p_id);
            }
        }


        $document->forcedelete();
        // $document->status = 2;   

        return redirect()->route('approve')
            ->with('success', 'Document rejected successfully');
    }

    public function export_script($id)
    {
        $data = ProductCode::where('doc_no', $id)->get();
        $insert_code_arr = array();
        // Get BRNACHES

        $branches_obj = DB::table('branches')->select('branch_id')
            ->where('branch_active', '=', "true")
            ->get();
        foreach ($branches_obj as $branch) {

            $branches[] = $branch->branch_id;
        }
        foreach ($data as $product) {
            if ($product->product_pack_flag == 0) {
                $product->product_pack_flag = 'G';
            } else if ($product->product_pack_flag == 1) {
                $product->product_pack_flag = 'G';
            }
            if ($product->type == 0) {
                $product->type = 3;
            } else if ($product->type == 1) {
                $product->type = 2;
            }
            if ($product->product_grade_id == 0) {
                $product->product_grade_id = 2;
            } else if ($product->product_grade_id == 1) {
                $product->product_grade_id = 1;
            }
            $per_script1 = "insert into master_data.master_product(lotserialexpireflag,product_pack_flag,vartype,commission_flag,product_type_flag,inactive,xposflag,xpostime,editflag,discflag,priceflag,cardcredit_charge,product_id,product_code,product_name1,product_name2,product_name_eng1,product_bill_name,main_product_unit_id,product_category_id,product_group_id,product_pattern_id,product_design_id,product_brand_id,product_type_id,product_grade_id) ";
            $script1 = "values(0,'" . $product->product_pack_flag . "',1,'N','" . $product->product_pack_flag . "','A','Y',now(),'N','Y','N',true," . "( select max(product_id+1) from master_data.master_product)," . $product->product_code_no . ",'" . $product->product_name . "','" . $product->product_name . "'," . "'" . $product->product_name . "'," . "'" . $product->product_name . " '," . $product->unit_id . "," . $product->category_id . "," . $product->group_id . "," . $product->pattern_id . "," . $product->design_id . "," . $product->brand_id . "," . $product->type . " , $product->product_grade_id );";
            $master_product = $per_script1 . $script1;

            $per_script2 = "insert into master_data.master_product_multiunit (product_id,product_code,product_unit_id,product_unit_rate,storage_unit_flag,stock_unit_flag )";
            $script2 = " values((select max(product_id) from master_data.master_product)," . $product->product_code_no . ", " . $product->unit_id . ",1,true,true);";
            $multiunit = $per_script2 . $script2;

            $per_script3 = "insert into master_data.master_product_multivendor (product_id,list_no,product_code,barcode_code,vendor_id,product_unit_id) ";
            $script3 = "values((select max(product_id) from master_data.master_product),1,$product->product_code_no " . "," . $product->product_code_no . "," . $product->supplier_id . "," . $product->unit_id . ");";
            $multivendor = $per_script3 . $script3;



            // $branches = Branch::where('branch_active', '=', "true")->get();

            // array(1,2,3,9,10,11,17,8,13,21,20,19,26);

            foreach ($branches as $branch) {
                $per_script4 = "insert into master_data.master_product_multiprice (product_id,product_code,barcode_code,product_unit_id,branch_id,listno,product_price1,product_price2,product_price3,product_price4,product_price5,product_price6,product_price7,product_price8,product_price9,product_price10)";
                $script4 = " values((select max(product_id) from master_data.master_product)," . $product->product_code_no . "," . $product->product_code_no . "," . $product->unit_id . "," . $branch . ",1,0,0,0,0,0,0,0,0,0,0);";
                $multiprice_arr[] = $per_script4 . $script4;
            }
            $multiprice = join("", $multiprice_arr);
            $multiprice_arr = array();
            $insert_code_arr[] = $master_product . $multiunit . $multivendor . $multiprice;



            // $insert_code_arr[] = $master_product . $multiunit . $multivendor . $multiprice;
        }

        return back()->withErrors($insert_code_arr);
    }

    ///// Start AutoComplete/////
    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('unit')
                ->where('product_unit_name', 'like', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative" required focus autocomplete="off">';
            foreach ($data as $row) {
                $output .= '<li id="vendor_list"><span class="d-none" id="unitID"> ' . $row->id . '</span><p id="unitName">' . $row->product_unit_code . '</p></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function vendorfetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('vendor')
                ->where('vendor_name', 'ilike', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '<li id="vendor_list"><span class="d-none" id="vendorID"> ' . $row->id . '</span><p id="vendorName">' . $row->vendor_name . '</p></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function brandfetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('brand')
                ->where('product_brand_name', 'ilike', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '
       <li id="brand_list"><span class="d-none" id="brandID"> ' . $row->id . '</span><p id="brandName">' . $row->product_brand_name . '</p></li>
       ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }


    ///// End Auto Complete/////

    /////Datatable/////
    public function manager_list(Request $request)
    {
        if ($request->ajax()) {
            $documents = Document::latest()->get();
            $data = array();
            foreach ($documents as $doc) {
                $codes = explode(",", $doc->pcode_id);
                $codes_count = count($codes);

                if ($doc->status == 0) {
                    $doc_status = 'Requested';
                } else if ($doc->status == 1) {
                    $doc_status = 'Approved';
                } else if ($doc->status == 3) {
                    $doc_status = 'Checked';
                } else {
                    $doc_status = 'Finished';
                }
                $user = User::where('id', $doc->prepared_by)->first();
                if ($user) {
                    $u_cat_arr =  explode(',', $user->mercate_id);
                    $categories  = Product_Category::whereIn('product_category_id', $u_cat_arr)->get();
                    if ($categories) {
                        $category_name_arr  = array();
                        foreach ($categories as $category) {
                            $category_name_arr[]  =  $category->product_category_name;
                        }
                        $category_name = implode(',', $category_name_arr);
                        $data_row_arr = array(
                            'id' => $doc->id,
                            'status' => $doc_status,
                            'document_no' => $doc->document_no,
                            'category_name' => $category_name,
                            'pcode_id'    => $codes_count,
                            'prepared_by' => $doc->prepared->name,
                            'prepared_at' => $doc->prepared_at,
                            'checked_by' => $doc->checked_by ? $doc->checked->name : "",
                            'checked_at' => $doc->checked_at,
                            'approved_by' => $doc->approved_by ? $doc->approved->name : "",
                            'approved_at' => $doc->approved_at,
                            'exported_by' => $doc->exported_by ? $doc->exported->name : "",
                            'exported_at' => $doc->exported_at
                        );
                        $data[] = (object) $data_row_arr;
                    }
                }
            }
            return DataTables()::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    // {{ route("approve_view", $document->id) }}
                    if ($data->status == 'Requested' || $data->status == 'Checked') {
                        $btn = '<a href="/approve_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    } else {
                        $btn = '<a href="/document_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('document_list_for_manager');
    }

    /////DataTabel For SD//
    public function sd_list(Request $request)
    {
        if ($request->ajax()) {
            $documents = Document::where('status', '=', 2)->whereNotNull('exported_by')->orderBy('exported_at', 'asc')->get();
            $data = array();
            foreach ($documents as $doc) {
                $codes = explode(",", $doc->pcode_id);
                $codes_count = count($codes);

                if ($doc->status == 0) {
                    $doc_status = 'Requested';
                } else if ($doc->status == 1) {
                    $doc_status = 'Approved';
                } else if ($doc->status == 3) {
                    $doc_status = 'Checked';
                } else {
                    $doc_status = 'Finished';
                }
                $user = User::where('id', $doc->prepared_by)->first();
                if ($user) {
                    $u_cat_arr =  explode(',', $user->mercate_id);
                    $categories  = Product_Category::whereIn('product_category_id', $u_cat_arr)->get();
                    if ($categories) {
                        $category_name_arr  = array();
                        foreach ($categories as $category) {
                            $category_name_arr[]  =  $category->product_category_name;
                        }
                        $category_name = implode(',', $category_name_arr);
                        $data_row_arr = array(
                            'id' => $doc->id,
                            'status' => $doc_status,
                            'document_no' => $doc->document_no,
                            'category_name' => $category_name,
                            'pcode_id'    => $codes_count,
                            'prepared_by' => $doc->prepared->name,
                            'prepared_at' => $doc->prepared_at,
                            'checked_by' => $doc->checked_by ? $doc->checked->name : "",
                            'checked_at' => $doc->checked_at,
                            'approved_by' => $doc->approved_by ? $doc->approved->name : "",
                            'approved_at' => $doc->approved_at,
                            'exported_by' => $doc->exported_by ? $doc->exported->name : "",
                            'exported_at' => $doc->exported_at
                        );
                        $data[] = (object) $data_row_arr;
                    }
                }
            }
            return DataTables()::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    // {{ route("approve_view", $document->id) }}
                    $btn = '<a class="btn btn-info open pr-3" href="{{route("export_script",$data->id)}}">Export <i class="fas fa-file-export"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('document_list_for_SD');
    }


    public function test(Request $request)
    {
        $checker = User::where('id', Auth::id())->first();

        $c_cat_arr =  explode(',', $checker->mercate_id);

        $check_categories  = Product_Category::whereIn('product_category_id', $c_cat_arr)->get();

        $documents = Document::latest()->get();
        $data = array();

        foreach ($documents as $doc) {
            $codes = explode(",", $doc->pcode_id);
            $codes_count = count($codes);

            if ($doc->status == 0) {
                $doc_status = 'Requested';
            } else if ($doc->status == 1) {
                $doc_status = 'Approved';
            } else if ($doc->status == 3) {
                $doc_status = 'Checked';
            } else {
                $doc_status = 'Finished';
            }
            $user = User::where('id', $doc->prepared_by)->first();
            if ($user) {
                $u_cat_arr =  explode(',', $user->mercate_id);
                $categories  = Product_Category::whereIn('product_category_id', $u_cat_arr)->get();

                if ($categories) {
                    $category_name_arr  = array();
                    foreach ($categories as $category) {
                        $category_name_arr[]  =  $category->product_category_name;
                    }
                    $category_name = implode(',', $category_name_arr);
                    $data_row_arr = array();
                    echo "checker :" . $checker->mercate_id . "</br>";
                    echo "user :" . $user->mercate_id . "</br>";
                    $checker_arr = explode(',', $checker->mercate_id);
                    $user_arr = explode(',', $user->mercate_id);
                    $test = array_intersect($checker_arr, $user_arr);

                    if (!empty($test)) {

                        echo "<pre>";
                        print_r($test);
                    }
                    //    dd($test);
                    if ($checker->mercate_id == $user->mercate_id) {

                        $data_row_arr = array(
                            'id' => $doc->id,
                            'status' => $doc_status,
                            'document_no' => $doc->document_no,
                            'category_name' => $category_name,
                            'pcode_id'    => $codes_count,
                            'prepared_by' => $doc->prepared->name,
                            'prepared_at' => $doc->prepared_at,
                            'checked_by' => $doc->checked_by ? $doc->checked->name : "",
                            'checked_at' => $doc->checked_at,
                            'approved_by' => $doc->approved_by ? $doc->approved->name : "",
                            'approved_at' => $doc->approved_at,
                            'exported_by' => $doc->exported_by ? $doc->exported->name : "",
                            'exported_at' => $doc->exported_at
                        );
                        $data[] = (object) $data_row_arr;
                    }
                }
            }
        }
        dd($test);
        // dd($data);
        die;
    }
    public function checker_list(Request $request)
    {
        if ($request->ajax()) {
            $checker = User::where('id', Auth::id())->first();
            $documents = Document::latest()->get();
            $data = array();
            foreach ($documents as $doc) {
                $codes = explode(",", $doc->pcode_id);
                $codes_count = count($codes);

                if ($doc->status == 0) {
                    $doc_status = 'Requested';
                } else if ($doc->status == 1) {
                    $doc_status = 'Approved';
                } else if ($doc->status == 3) {
                    $doc_status = 'Checked';
                } else {
                    $doc_status = 'Finished';
                }
                $user = User::where('id', $doc->prepared_by)->first();
                if ($user) {
                    $u_cat_arr =  explode(',', $user->mercate_id);
                    $categories  = Product_Category::whereIn('product_category_id', $u_cat_arr)->get();
                    if ($categories) {
                        $category_name_arr  = array();
                        foreach ($categories as $category) {
                            $category_name_arr[]  =  $category->product_category_name;
                        }
                        $category_name = implode(',', $category_name_arr);
                        $checker_arr = explode(',', $checker->mercate_id);
                        $user_arr = explode(',', $user->mercate_id);
                        $cat_intersect = array_intersect($checker_arr, $user_arr);

                        if (!empty($cat_intersect)) {

                            $data_row_arr = array(
                                'id' => $doc->id,
                                'status' => $doc_status,
                                'document_no' => $doc->document_no,
                                'category_name' => $category_name,
                                'pcode_id'    => $codes_count,
                                'prepared_by' => $doc->prepared->name,
                                'prepared_at' => $doc->prepared_at,
                                'checked_by' => $doc->checked_by ? $doc->checked->name : "",
                                'checked_at' => $doc->checked_at,
                                'approved_by' => $doc->approved_by ? $doc->approved->name : "",
                                'approved_at' => $doc->approved_at,
                                'exported_by' => $doc->exported_by ? $doc->exported->name : "",
                                'exported_at' => $doc->exported_at
                            );
                            $data[] = (object) $data_row_arr;
                        }
                    }
                }
            }
            return DataTables()::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    // {{ route("approve_view", $document->id) }}
                    if ($data->status == 'Requested') {
                        $btn = '<a href="/approve_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    } else {
                        $btn = '<a href="/document_view/' . $data->id . '" class="edit btn btn-primary btn-sm">View</a>';
                        return $btn;
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('document_list_for_manager');
    }
}
