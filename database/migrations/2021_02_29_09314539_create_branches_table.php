<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //  Schema::table('branches', function (Blueprint $table) {
        //     $table->string('name')->change()->nullable();
        //     $table->string('short_name')->change()->nullable();;
        //     $table->string('address')->change()->nullable();;
        //   });
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('branch_id')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('name')->nullable();;
            $table->string('short_name')->nullable();;
            $table->string('address')->nullable();;
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->string('fb_link')->nullable();
            $table->string('branch_active')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
