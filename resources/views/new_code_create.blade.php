<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRO 1 ERP New Code | Create</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>

<body class="">
    <div class="container-fluid ">
        <nav class="navbar navbar-light  ">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="https://pro1globalhomecenter.com/wp-content/uploads/2020/04/cropped-PRO-1-Global-Logo-512x172-RGB.png" width="204" height="68" class="d-inline-block align-top" alt="PRO1 ONLINE STORE">
                </a>
                <div class="navbar-nav" id="navbarText">
                    <span class="text-uppercase align-center m-auto navbar-text">Create New ERP Code </span>
                </div>
            </div>
        </nav>
        <div class="row mb-3 ">
            <div class="col-md-6 col-sm-12 col-lg-9 ">
                <div class="bg-light p-3">
                    <div class="row mb-3">
                        <div class="col-3">Document No</div>
                        <div class="col-9">NCR_03_02_2020-0001</div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">User Name</div>
                        <div class="col-9">Khin Sandar Aye</div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Document Date</div>
                        <div class="col-9">2020-02-03</div>
                        <!-- <div class="col-6"><input class="form-control" type="date" name="doc_date" id="doc_date" value="2020-02-03" min="2000-01-01" max="2020-02-12" /></div> -->
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Approved by </div>
                        <div class="col-9">Ye Win Maung</div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-3">Approved Date </div>
                        <div class="col-9">2020-02-03</div>
                    </div>
                   <input type="submit">
                </div>
            </div>

            <!-- offset-md-6   offset-lg-9  -->
            <div class="col-md-6 col-sm-12 col-lg-3">
                <form action="" class="table-responsive">
                    <table class="table table-striped table-hover form-control my-3">
                        <thead>
                            <th scope="col" colspan=2 class="-uppercase text-center">Return Exchange Product</th>

                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <span class="ps-5"></span>
                                    <span class="ps-5"></span>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <th><small>Now you are login as: </small></th>
                                            <td class="ps-3"> Khin Sandar Aye </td>
                                        </tr>
                                        <tr>
                                            <th><small>Date Time : </small>
                                            <td class="ps-3"> <em>2020-02-03</em> </td>
                                        </tr>
                                        <tr>
                                            <td> <button class="btn btn-success m-3">Save</button>

                                            </td>
                                            <td>
                                                <button class="btn btn-danger m-3">Cancel</button>

                                            </td>
                                        </tr>


                                    </table>
                                </td>

                            </tr>

                        </tbody>
                    </table>
            </div>
            </form>
        </div>
        <table class="table table-striped table-hover table-bordered bg-white">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Point Type</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Product Code</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Unit</th>
                    <th scope="col">Supplier Code</th>
                    <th scope="col">Category Code</th>
                    <th scope="col">Group Code</th>
                    <th scope="col">Pattern Code</th>
                    <th scope="col">Design Code</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Supplier Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>HIP</td>
                    <td>Local</td>
                    <td>2000000302454</td>
                    <td>Aladdin UPVC Fixe & Casement Window 80mm Frame (W)2ft x (H)5ft Sola Green (CM)</td>
                    <td>PC</td>
                    <td>VEN-001061</td>
                    <td>08-DW</td>
                    <td>08-DW06</td>
                    <td>08-DW0601</td>
                    <td>08-DW060106</td>
                    <td>Aladdin</td>
                    <td>U MYINT THU Construction Group Co;Ltd</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>HIP</td>
                    <td>Local</td>
                    <td></td>
                    <td>Aladdin UPVC Fixed Window (W)4.5ft x (H)1ft 2in Sola Green (CM)</td>
                    <td>PC</td>
                    <td>VEN-001061</td>
                    <td>08-DW</td>
                    <td>08-DW06</td>
                    <td>08-DW0601</td>
                    <td>08-DW060106</td>
                    <td>Aladdin</td>
                    <td>U MYINT THU Construction Group Co;Ltd</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>HIP</td>
                    <td>Local</td>
                    <td></td>
                    <td>Aladdin UPVC Fixed Window (W)3.5ft x (H)1ft 2in Sola Green (CM)</td>
                    <td>PC</td>
                    <td>VEN-001061</td>
                    <td>08-DW</td>
                    <td>08-DW06</td>
                    <td>08-DW0601</td>
                    <td>08-DW060106</td>
                    <td>Aladdin</td>
                    <td>U MYINT THU Construction Group Co;Ltd</td>
                </tr>

            </tbody>
        </table>
    </div>
</body>

</html>