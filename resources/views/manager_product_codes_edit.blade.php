<!DOCTYPE html>
@extends('layouts.app')
@section('content')
<div class="row mb-3 ">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">Edit New Code (ERP) </h1>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-9 bg-white">
        <form action="{{ route('manager_product_codes_update',$product->id) }}" method="POST" >
            @csrf
            <div class="row mt-3">
                <div class="offset-xs-10 offset-sm-10 offset-md-10 col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group text-right">
                        <input type="hidden" name="product_id" value='{{$product->id}}'>
                        <button type="submit" class="btn btn-primary ">Save <i class="fas fa-save "></i></button>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <div class="form-group">
                        <label class="control-label"> Product Code Type <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >{{ $product_Code->product_pack_flag === 0 ? 'Product Code' : 'FOC' }}</p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Prodcut Code:<span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >{{ $product->product_code_no }}</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label class="control-label"> Name: <span class="text-danger">*</span>:</label>
                        <input type="text" name="product_name" value="{{ $product->product_name }}" class="form-control" autofocus placeholder="Name">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Product Type: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >{{ $product->type === 0 ? 'HIP' : 'Structure' }} </p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Grade: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >{{ $product_Code->type === 0 ? 'Local' : 'Import' }}</p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Category: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}
                        </p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Group: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}
                        </p>

                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Pattern: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}
                        </p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Design: <span class="text-danger">*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}
                        </p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Unit <span class="text-danger" required focus>*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}
                        </p>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Supplier <span class="text-danger" required focus>*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}
                        </p>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <label class="control-label"> Brand <span class="text-danger" required focus>*</span>:</label>
                        <p class="form-control" readonly >
                            {{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}
                        </p>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@endsection


@section('js')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
@endsection