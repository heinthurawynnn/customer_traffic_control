@extends('layouts.app')
@section('content')
<form action="{{ route("admin.products.store") }}" method="POST">
@csrf
@foreach($properties as $property)
<tr>
    <td>
        {{ $property->name ?? '' }}
    </td>
    <td>
        {{ $property->price ?? '' }}
    </td>
    <td>
        @foreach ($property->properties as $property)
        <b>{{ $property['key'] }}</b>: {{ $property['value'] }}<br />
        @endforeach
    </td>
</tr>
</form>
@endsection