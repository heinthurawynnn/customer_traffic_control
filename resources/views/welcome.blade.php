<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRO 1 Supplier Return</title>
    <!-- CSS only -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
    <!-- JavaScript Bundle with Popper -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> -->
</head>

<body class="">
    <div class="container-fluid ">
        <nav class="navbar navbar-light bg-info mb-5 ">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="https://pro1globalhomecenter.com/wp-content/uploads/2020/04/cropped-PRO-1-Global-Logo-512x172-RGB.png" width="204" height="68" class="d-inline-block align-top" alt="PRO1 ONLINE STORE">
                </a>
                <div class="navbar-nav" id="navbarText">
                    <span class="text-uppercase align-center m-auto navbar-text">RTN Document</span>
                </div>
            </div>
        </nav>

        <div class="row table-responsive">

            <div class=" offset-md-6 col-md-6 offset-lg-9 col-sm-12 col-lg-3">
                <form action="">
                    <table class="table table-striped table-hover form-control my-3">
                        <thead>
                            <th scope="col" colspan=2 class="-uppercase text-center">Return Exchange Product</th>
                        </thead>
                        <td class="btn-group align-right" role="group" aria-label="Basic mixed styles example">
                            <button type="button" class="btn btn-sm btn-success">Save</button>
                            <button type="button" class="btn btn-sm btn-warning">Save Draft</button>
                            <button type="button" class="btn btn-sm btn-danger">Delete</button>
                        </td>
                        <tbody>
                            <tr>
                                <th scope="row">Document No.</th>
                                <td>RTNLAN120314-0001</td>
                            </tr>
                            <tr>
                                <th scope="row">Branch</th>
                                <td>Lanthit</td>
                            </tr>
                            <tr>
                                <th scope="row">Supplier Code</th>
                                <td><input class="form-control" type="text" name="supplier_code" id="supplier_code" value="T-0003" /></td>
                            </tr>
                            <tr>
                                <th scope="row">Supplier Name</th>
                                <td>TMW Enterprise Limited</td>
                            </tr>
                            <tr>
                                <th scope="row">Requested by Code</th>
                                <td>Wah Wah Aung</td>
                            </tr>
                            <tr>
                                <th scope="row">Document Date</th>
                                <td><input class="form-control" type="date" name="doc_date" id="doc_date" value="2020-02-12" min="2000-01-01" max="2020-02-12" /></td>
                            </tr>
                            <tr>
                                <th scope="row">Exchange No</th>
                                <td><input class="form-control" type="text" name="exchange_no" id="exchange_no" value="7549" /></td>
                            </tr>
                            <tr>
                                <th scope="row">Attach File</th>
                                <td><input class="form-control" type="file" name="exchange_no_file" id="exchange_no_file" /></td>
                            </tr>
                            <tr>
                                <th scope="row">Remark</th>
                                <td><textarea type="text" name="remark" id="remark" class="form-control"> Supplier Return</textarea></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
            </form>
        </div>

        <table class="table table-striped table-hover table-bordered bg-white">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Product Code</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Unit</th>
                    <th scope="col">PO Doc. No</th>
                    <th scope="col">Stock Qty</th>
                    <th scope="col">Return Qty</th>
                    <th scope="col">Confirmed Qty</th>
                    <th scope="col">CN Doc. No</th>
                    <th scope="col">Remark</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td><input class="form-control" type="number" name="product_code" id="product_code" value="008562001060" /></td>
                    <td>(Discon) SONY Battery SUM-2NUP2A C-Size 2PC (Shrink Pack)</td>
                    <td>PC</td>
                    <td><select class="form-select" aria-label="po_doc_no" name="po_doc_no" id="po_doc_no">
                            <option>-- Select PO Doc. No --</option>
                            <option value="POLAN11900612-0001" selected>POLAN11900612-0001</option>
                            <option value="POLAN11900612-0001">POLAN11900612-0003</option>
                            <option value="POLAN11900612-0001">POLAN11900619-0001</option>
                        </select>
                    </td>
                    <td>31</td>
                    <td>3</td>
                    <td>2</td>
                    <td><input class="form-control" type="file" name="cn_doc_no" id="cn_doc_no" /></td>
                    <td><textarea type="text" name="remark" id="remark" class="form-control"> Supplier Return</textarea></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><input class="form-control" type="number" name="product_code" id="product_code" value="008562001060" /></td>
                    <td>(Discon) SONY Battery SUM-2NUP2A C-Size 2PC (Shrink Pack)</td>
                    <td>PC</td>
                    <td><select class="form-select" aria-label="po_doc_no" name="po_doc_no" id="po_doc_no">
                            <option>-- Select PO Doc. No --</option>
                            <option value="POLAN11900612-0001" selected>POLAN11900612-0001</option>
                            <option value="POLAN11900612-0001">POLAN11900612-0003</option>
                            <option value="POLAN11900612-0001">POLAN11900619-0001</option>
                        </select>
                    </td>
                    <td>31</td>
                    <td>3</td>
                    <td>2</td>
                    <td><input class="form-control" type="file" name="cn_doc_no" id="cn_doc_no" /></td>
                    <td><textarea type="text" name="remark" id="remark" class="form-control"> Supplier Return</textarea></td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><input class="form-control" type="number" name="product_code" id="product_code" value="008562001060" /></td>
                    <td>(Discon) SONY Battery SUM-2NUP2A C-Size 2PC (Shrink Pack)</td>
                    <td>PC</td>
                    <td><select class="form-select" aria-label="po_doc_no" name="po_doc_no" id="po_doc_no">
                            <option>-- Select PO Doc. No --</option>
                            <option value="POLAN11900612-0001" selected>POLAN11900612-0001</option>
                            <option value="POLAN11900612-0001">POLAN11900612-0003</option>
                            <option value="POLAN11900612-0001">POLAN11900619-0001</option>
                        </select>
                    </td>
                    <td>31</td>
                    <td>3</td>
                    <td>2</td>
                    <td><input class="form-control" type="file" name="cn_doc_no" id="cn_doc_no" /></td>
                    <td><textarea type="text" name="remark" id="remark" class="form-control"> Supplier Return</textarea></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>