@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show product_Category</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Categories.index') }}"> Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Category Id:</strong>
                {{ $product_Category->product_category_id }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Category Code:</strong>
                {{ $product_Category->product_category_code }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Category Name:</strong>
                {{ $product_Category->product_category_name }}    
            </div>
        </div>
    </div>
@endsection