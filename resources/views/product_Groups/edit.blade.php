@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product Group</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Groups.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('product_Groups.update',$product_Group->id) }}" method="POST">
    	@csrf
        @method('PUT')


         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Category Id:</strong>
		            <input type="text" name="product_group_id" value="{{ $product_Group->product_group_id }}" class="form-control" placeholder="Product Group Id">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Group Code:</strong> 
		            <input type="text" name="product_group_code" value="{{ $product_Group->product_group_code }}" class="form-control" placeholder="Product Group Code">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Group Name:</strong>
		            <input type="text" name="product_group_name" value="{{ $product_Group->product_group_name }}" class="form-control" placeholder="Product Group Name">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>


@endsection