@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show product_Group</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Groups.index') }}"> Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Group Id:</strong>
                {{ $product_Group->product_group_id }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Group Code:</strong>
                {{ $product_Group->product_group_code }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Group Name:</strong>
                {{ $product_Group->product_group_name }}    
            </div>
        </div>
    </div>
@endsection