<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'PRO1' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/jpg" href="{{asset('assets/favico.png')}}" />

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('assets/logo.png')}}" class="img-responsive" width="150px" height="auto">

                    <!-- {{ config('app.name', 'Laravel') }} -->
                </a>
                <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto"></ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                    @hasrole('manager')
                    <li><a class="nav-link" href="{{ route('create') }}">{{ __('Demo For Merchandise Admin') }}</a></li>
                 
                    @endhasrole
                    @hasrole('merchandise')
                    <li><a class="nav-link" href="{{ route('prepare') }}">{{ __('Demo For Merchandise User') }}</a></li>
                 
                    @endhasrole

                        @guest
                        <li><a class="nav-link" href="{{ route('user_login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('user_register') }}">{{ __('Register') }}</a></li>

                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>


                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @hasrole('admin')
                                <a class="dropdown-item" href="{{ route('admin') }}">Admin Dashboard</a>
                                <a class="dropdown-item" href="{{ route('roles.index') }}">Manage Role</a>

                                @else
                                <a class="dropdown-item" href="{{ route('create') }}"> <span class="far fa-heart text-danger">Create New Document</span></a>

                                @endhasrole
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="far fa-heart text-danger">
                                        {{ __('Logout') }}

                                    </span>
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </li>
                        @endguest
                </div>
            </div>
        </nav>

        <main class="">
            <div class="container-fluid">
                @yield('content')
            </div>
        </main>
    </div>
</body>

</html>