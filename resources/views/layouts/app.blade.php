<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'PRO1' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>




    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/jpg" href="{{asset('assets/favico.png')}}" />
    <!-- Data table -->

    <link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>


</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('assets/logo.png')}}" class="img-responsive" width="150px" height="auto">

                    <!-- {{ config('app.name', 'Laravel') }} -->
                </a>
                <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto"></ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        
                        @hasrole('Manager')
                        <li><a class="nav-link" href="{{ route('approve') }}">{{ __('Document List') }} <i class="fas fa-file-alt"></i></a></li>
                        @endhasrole
                        @hasrole('Checker')
                        <li><a class="nav-link" href="{{ route('checker') }}">{{ __('Document List') }} <i class="fas fa-file-alt"></i></a></li>
                        @endhasrole
                        @hasrole('Merchandise')
                        <li><a class="nav-link" href="{{ route('prepare') }}">{{ __('Request New Document') }} <i class="fas fa-file-alt"></i></a></li>
                        <li><a class="nav-link" href="{{ route('documents_list') }}">{{ __('Requested Document Lists') }} <i class="fas fa-file-archive"></i></a></li>
                        @endhasrole
                        @hasrole('SD')
                        <li><a class="nav-link" href="{{ route('export') }}">{{ __('Document List') }} <i class="fas fa-file-alt"></i></a></li>
                        <li><a class="nav-link" href="{{ route('user_register') }}">{{ __('Register System User') }} <i class="fas fa-users"></i></a></li>
                        @endhasrole
                        
                        @hasrole('Ctl-Manager')
                        <li><a class="nav-link" href="{{ route('customers.index') }}">Customer Traffic <i class="fas fa-users"></i> <i class="fas fa-chart-line "></i></a>
                        </li>
                        @endhasrole
                        @hasrole('Ctl-User')
                        <li><a class="nav-link" href="{{ route('customers.index') }}">Customer Traffic <i class="fas fa-users"></i> <i class="fas fa-chart-line "></i></a>
                        </li>
                        @endhasrole
                        @hasrole('Ctl-SD')
                        <li><a class="nav-link" href="{{ route('customers.index') }}">Customer Traffic <i class="fas fa-users"></i> <i class="fas fa-chart-line "></i></a>
                        </li>
                        @endhasrole
                        @guest
                        <li><a class="nav-link" href="{{ route('user_login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('user_register') }}">{{ __('Register') }}</a></li>

                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>


                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @hasrole('admin')
                                <a class="dropdown-item" href="{{ route('admin') }}">Admin Dashboard</a>
                                <a class="dropdown-item" href="{{ route('roles.index') }}">Manage Role</a>

                                @endhasrole

                                <a class="dropdown-item" href="{{ route('user_reset_pwd') }}">
                                    <span class="fas fa-key text-info">
                                        {{'Reset Password'}}</span></a>


                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="fas fa-power-off text-danger">

                                        {{ __('Logout') }}

                                    </span>
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </li>
                        @endguest
                </div>
            </div>
        </nav>

        <main class="container-fluid main-conent p-3 pb-3">
            @yield('content')
        </main>

    </div>
</body>
@yield('js')

</html>