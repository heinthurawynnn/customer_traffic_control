<!DOCTYPE html>
@extends('layouts.app')

@section('content')

@php
$pcount = count($products);
$pcode_ids = "";
if ($document->status == 0) {
$doc_status = 'Requested';
} else if ($document->status == 1) {
$doc_status = 'Approved';
} else {
$doc_status = 'Finished';
}
@endphp

<div class="row">
    <div class="col-md-12  text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto"> {{$doc_status}} Document </h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12 col-lg-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="bg-light p-3 my-3 card">
            <div class="row mb-3">

                <div class="col">Document No :</div>
                <div class="col">
                    <input type="text" disabled name="document_no" value="{{$document->document_no}}" />
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">Requested by :</div>
                <div class="col">
                    {{$document->prepared->name}}
                </div>
            </div>

            <div class="row mb-3">
                <div class="col">Document Prepared Date</div>
                <div class="col"> {{$document->prepared_at}}</div>
            </div>
            <div class="row mb-3">
                <div class="col">Approved by :</div>
                <div class="col">
                    {{$document->approved_by ? $document->approved->name : '-'}}
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">Approved At :</div>
                <div class="col">
                    {{$document->approved_at ? $document->approved_at : '-'}}
                </div>
            </div>

        </div>

    </div>

</div>
<table class="table table-striped table-hover table-bordered bg-white ">
    <tr>
        <th>No</th>
        <th>Type</th>
        <th>Product Code No</th>
        <th>Product Name</th>
        <th>Category</th>
        <th>Group</th>
        <th>Pattern</th>
        <th>Design</th>
        <th>Supplier</th>
        <th>Brand</th>
        <th>Unit</th>
        <th>Product Pack Flag</th>
    </tr>

    @foreach ($products as $product)
    <tr>
        <td>{{ ++$i}}</td>
        <td>{{ $product->type === 0 ? 'HIP' : 'Structure' }}</td>
        <td>{{ $product->product_code_no }}</td>
        <td>{{ $product->product_name }}</td>
        <td>{{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}</td>
        <td>{{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}</td>
        <td>{{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}</td>
        <td>{{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}</td>
        <td>{{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}</td>
        <td>{{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}</td>
        <td>{{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}</td>

        <td> @if ($product->product_pack_flag == 0)
            {{ 'Product Code' }}
            @else
            {{ 'FOC' }}
            @endif
        </td>
        @php
        $pcode_ids = $pcode_ids .",". $product->id;
        @endphp



    </tr>
    @endforeach
</table>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        var doc_id = $("#doc_id").val();
        var pcode_ids = '<?php echo $pcode_ids; ?>';
        $("#pcode_ids").val(pcode_ids);
        <?php if ($errors->any()) { ?>
            $("#pcode").removeClass("d-none");
            $("#pcode").addClass("d-block");
            $("#modal_btn").addClass("d-none");
        <?php } ?>
        $('#modify_btn').click(function() {
            $("#barcode").addClass('d-block');
            $("#barcode").addClass('d-block');
            var barcode = $("#barcode").val();
            if (barcode == '') {
                $.ajax({
                    url: '/pcode_generate/',
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }
                        if (len > 0) {
                            var pcode = response.data;
                            $("#generated_barcode").val(pcode);
                            $("#generated_barcode_p").text(pcode);
                            $("#generated_barcode_p").removeClass('d-none');
                            $("#barcode").addClass('d-none');
                        }
                    }
                })
            }
            $("#pcode_gen").addClass('disabled');
            return false;
        });
        var catid = $('#category_id').val();
        $('#group_id').find('option').not(':first').remove();
        $.ajax({
            url: '/product_Codes/cat/' + catid,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                console.log(response.data);
                var len = 0;
                if (response.data != null) {
                    len = response.data.length;
                }
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var id = response.data[i].product_group_id;
                        var name = response.data[i].product_group_code + " | " + response.data[i].product_group_name;
                        // console.log(name);
                        var option = "<option value='" + id + "'>" + name + "</option>";
                        $("#group_id").append(option);
                    }
                }
            }
        });
        $('#group_id').change(function() {
            var id = $(this).val();
            $('#pattern_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/gp/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }

                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_pattern_id;
                            var name = response.data[i].product_pattern_code + " | " + response.data[i].product_pattern_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#pattern_id").append(option);
                        }
                    }
                }
            })
        });
        $('#pattern_id').change(function() {
            var id = $(this).val();
            $('#design_id').find('option').not(':first').remove();
            $.ajax({
                url: '/product_Codes/ptn/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }

                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_design_id;
                            var name = response.data[i].product_design_code + " | " + response.data[i].product_design_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#design_id").append(option);
                        }
                    }
                }
            })
        });

        /////Autocomplete//////
        $('#product_unit_name').keyup(function() {
            var query = $(this).val();
            var _token = $('input[name="_token"]').val();

            if (query != '') {
                $.ajax({
                    url: "{{ route('autocomplete.fetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_unit_name_list').fadeIn();
                        $('#product_unit_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', $('#unit_list'), function() {
            $('#product_unit_id').val($('#unitID').text());
            $('#product_unit_name').val($('#unitName').text());
            $('#product_unit_name_list').fadeOut();
        });


        $('#product_vendor_name').keyup(function() {
            var query = $(this).val();
            var _token = $('input[name="_token"]').val();

            if (query != '') {
                $.ajax({
                    url: "{{ route('autocomplete.vendorfetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_vendor_name_list').fadeIn();
                        $('#product_vendor_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', '#vendor_list', function() {
            $('#product_vendor_id').val($('#vendorID').text());
            $('#product_vendor_name').val($('#vendorName').text());
            $('#product_vendor_name_list').fadeOut();
        });

        $('#product_brand_name').keyup(function() {
            var query = $(this).val();
            if (query != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('autocomplete.brandfetch') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {
                        $('#product_brand_name_list').fadeIn();
                        $('#product_brand_name_list').html(data);
                    }
                });
            }
        });
        $(document).on('click', '#brand_list', function() {
            $('#product_brand_id').val($('#brandID').text());
            $('#product_brand_name').val($('#brandName').text());
            $('#product_brand_name_list').fadeOut();
        });
    });
</script>
@endsection