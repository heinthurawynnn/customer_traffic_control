@extends('layouts.app')
@section('content')
<div class="container bg-light">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Departments</h2>
            </div>
            <div class="pull-right mb-3">
                @can('field-create')
                <a class="btn btn-success" href="{{ route('fields.create') }}"> Add New Customer Field</a>
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        @foreach ($fields as $field)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $field->name }}</td>
            <td> @if ($field->type == 0)
                {{ 'Customer' }}
                @else
                {{ 'Vehical' }}
                @endif
            </td>
            <td>
                <form action="{{ route('fields.destroy',$field->id) }}" method="POST">
                    @can('field-edit')
                    <a class="btn btn-primary" href="{{ route('fields.edit',$field->id) }}">Edit</a>
                    @endcan
                    @csrf
                    @method('DELETE')
                    @can('field-delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection