@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Customer</title>
    <meta charset="utf-8">
</head>

<body>
    <div class="row bg-light">
    </div>
    <div class="container mt-4">
        @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header text-center font-weight-bold">
                Customer Record Data
            </div>
            <div class="card-body">
                <form action="{{ route('fields.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" id="name" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Type</label>
                        <select class="form-control" name="type" required focus>
                            <option value="">{{ '--Customer ? Vehical--' }}</option>
                            @if (!empty(old('type')) && old('type') == 0)
                            <option value="0" selected>{{ 'Customer' }}</option>
                            <option value="1">{{ 'Vehical' }}</option>
                            @elseif(!empty(old('type')) && old('type') == 1)
                            <option value="0">{{ 'Customer' }}</option>
                            <option value="1" selected>{{ 'Vehical' }}</option>
                            @else
                            <option value="0">{{ 'Customer' }}</option>
                            <option value="1">{{ 'Vehical' }}</option>
                            @endif
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    </form>
</body>

</html>
@endsection