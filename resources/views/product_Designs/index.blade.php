@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Product Design</h2>
        </div>
        <div class="pull-right mb-3">
            @can('product-design-create')
            <a class="btn btn-success" href="{{ route('product_Designs.create') }}"> Add New Design</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Product Design Id</th>
        <th>Product Design Code</th>
        <th>Product Design Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($product_Designs as $product_Design)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $product_Design->product_design_id }}</td>
        <td>{{ $product_Design->product_design_code }}</td>
        <td>{{ $product_Design->product_design_name }}</td>
      
        <td>
            <form action="{{ route('product_Designs.destroy',$product_Design->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('product_Designs.show',$product_Design->id) }}">Show</a>
                @can('product-design-edit')
                <a class="btn btn-primary" href="{{ route('product_Designs.edit',$product_Design->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('product-design-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $product_Designs->links() !!}


@endsection