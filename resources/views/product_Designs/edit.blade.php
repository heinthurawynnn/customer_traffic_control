@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Design Group</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Designs.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('product_Designs.update',$product_Design->id) }}" method="POST">
    	@csrf
        @method('PUT')


         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Design Id:</strong>
		            <input type="text" name="product_design_id" value="{{ $product_Design->product_design_id }}" class="form-control" placeholder="Product Design Id">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Design Code:</strong> 
		            <input type="text" name="product_design_code" value="{{ $product_Design->product_design_code }}" class="form-control" placeholder="Product Design Code">
		        </div>
		    </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Product Design Name:</strong>
		            <input type="text" name="product_design_name" value="{{ $product_Design->product_design_name }}" class="form-control" placeholder="Product Design Name">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>


    </form>


@endsection