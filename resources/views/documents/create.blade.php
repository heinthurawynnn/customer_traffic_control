@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Product Document</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('documents.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Document No:</strong>
                <input type="text" name="document_no" class="form-control" value="{{ old('document_no') }}" placeholder="Document No">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Code:</strong>
                <input type="text" name="pcode_id" class="form-control" value="{{ old('pcode_id') }}" placeholder="Product Code">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                <input type="text" name="status" class="form-control" value="{{ old('status') }}" placeholder="Status">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Prepare By:</strong>
                <input type="text" name="prepared_by" class="form-control" value="{{ old('prepared_by') }}" placeholder="Prepare By">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Checked At:</strong>
                <input type="text" name="checked_at" class="form-control" value="{{ now() }}" placeholder="Checked At">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Checked By:</strong>
                <input type="text" name="checked_by" class="form-control" value="{{ old('checked_by') }}" placeholder="Checked By">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Prepare At:</strong>
                <input type="text" name="prepared_at" class="form-control" value="{{ now() }}" placeholder="Prepare At">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Approved By:</strong>
                <input type="text" name="approved_by" class="form-control" value="{{ old('approved_by') }}" placeholder="Approved By">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Approved At:</strong>
                <input type="text" name="approved_at" class="form-control" value="{{ now() }}" placeholder="Approved At">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Exported By:</strong>
                <input type="text" name="exported_by" class="form-control" value="{{ old('exported_by') }}" placeholder="Exported By">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Exported At:</strong>
                <input type="text" name="exported_at" class="form-control" value="{{ old('exported_at') }}" placeholder="Exported At">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection