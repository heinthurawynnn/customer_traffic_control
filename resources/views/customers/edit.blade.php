@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col">
        <div class="container mt-4">
            @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header text-center font-weight-bold bg-primary">
                    <h1 class="text-light text-uppercase"> Customer Traffic Record Data <a href="{{ route('customers.index') }}" title="Customer Traffic Record Data"> <i class="fas fa-chart-line text-warning"></i></a></h1>
                </div>
                <div class="card-body bg-info">
                    <form action="{{ route('customers.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-light"> <strong class="text-primary">Save</strong> <i class="fas fa-save text-primary"></i></button>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <strong class="text-light">Branch Code :</strong>
                                    <select class="form-control" id="branch_id" name="branch_id" required focus>
                                        <option value="" disabled selected>-- Please Select Branch --</option>
                                        @foreach($branches as $branch)
                                        @php $branch_count = count($branches);@endphp

                                        <option value="{{$branch->branch_id}}" {{ $branch_count == 1  ? 'selected': '' }}{{ old('branch_id') == $branch->id ? 'selected': '' }}>{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <strong class="text-light">Date :</strong>
                                    <input type="date" id="date_client" name="date_client" class="form-control" value={{now()}} required>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <strong class="text-light">Time Period :</strong>
                                    <select class="form-control" id="time" name="time" required focus>
                                        <option value="" disabled selected>-- Please Select Time Period --</option>
                                        @foreach($periods as $period)
                                        <option value="{{$period->time}}" {{ old('time') == $period->time ? 'selected': '' }}>{{ $period->time }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach($fields as $field)
                            <div class="col-6">
                                <strong class="text-light">Type : {{$field->type == 0 ? 'Customer': 'Vehicle'}}</strong>
                                <div class="form-group">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{{$field->name}}</span>
                                        </div>
                                        <input type="number" min=0 class="form-control" placeholder="No of {{$field->name}}" id="{{$field->name}}" name="{{$field->type == 0 ? 'Customer': 'Vehicle'}}-{{$field->name}}" value="0">
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$( document ).ready(function() {
    date_client.max = new Date().toISOString().split("T")[0];

    // console.log( "ready!" );
});
</script>
@endsection