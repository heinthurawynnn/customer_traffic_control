@extends('adminlte::auth.auth-page', ['auth_type' => 'register'])

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )

@if (config('adminlte.use_route_url', false))
@php( $login_url = $login_url ? route($login_url) : '' )
@php( $register_url = $register_url ? route($register_url) : '' )
@else
@php( $login_url = $login_url ? url($login_url) : '' )
@php( $register_url = $register_url ? url($register_url) : '' )
@endif

@section('auth_header', __('Enter Your OTP'))

@section('auth_body')

<form action="{{ route('otp_check') }}" method="POST">
    @csrf
    {{ csrf_field() }}
    {{-- Card holder field --}}
    <div class="input-group mb-3">
        <input type="hidden" name="otp" value="{{$otp}}" >
        <input type="hidden" name="ph_no" Value="{{$ph_no}}">
        <input type="text" name="confirm-otp" class="form-control {{ $err != NULL ? 'is-invalid' : '' }}" value="{{ old('confirm-otp') ?  old('confirm-otp')  :  $otp}}" placeholder="{{ $err }}" autofocus>
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="text-danger" title="Required"> * </span>
                <span class="fas fa-credit-card"></span>
            </div>
        </div>
        @if($errors->has('cityloft_id'))
        <div class="invalid-feedback">
            <strong>{{ $errors->first('confirm-otp') }}</strong>
        </div>
        @endif
    </div>
    {{-- Register button   --}}
        <button type="submit" class="btn btn-block btn-flat btn-primary">
            <span class="fas fa-user-plus"></span>
            {{ __('Confirm OTP') }}
    </button>
</form>
@stop
