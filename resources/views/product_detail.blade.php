@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col py-5">
        <div class="container bg-white py-3">
            <div class="row ">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Show Product</h2>
                    </div>
                    <div class="pull-right mb-3">
                        <a class="btn btn-primary" href="{{ route('home') }}"> Back</a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <img src="{{asset('assets/product_upload_img/'.$product->img_path)}}" class="img-fluid" alt="{{$product->img_path}}" title="{{$product->img_path}}">

                </div>

                <div class="col-md-8 col-sm-12">
                    <div class="card-body">
                        <h5 class="card-title"> {{ $product->name }} </h5>
                        <p class="card-text">
                            <strong class="card-text"> Promotion Price : {{ $product->sale_price }} </strong>
                            <br />
                            <em class="card-text"> Normal Price : <del>{{ $product->normal_price }}</del></em>
                        </p>
                        <p class="text-muted">{{ $product->category_id ? $product->category->name : 'Uncategorized' }}</p>
                        <p class="card-text"> Barcode : {{ $product->barcode }}</p>
                        <p class="card-text"> Promotion : {{ $product->sale_from }} - {{ $product->sale_to }}</p>
                        <p class="card-text"> Brand: {{ $product->brand_id ? $product->brand->name : 'Uncategorized Brand' }}</p>
                        <div class="card-title font-weight-bold">Short Descripiton </div>
                        <p class="card-text"> {{ $product->short_desc }}</p>
                        <div class="card-title font-weight-bold">Descripiton </div>
                        @if(isset($product->description))
                            @php
                        $bodytag = str_replace("♦", "</br><p>\r\n ♦ ", $product->description);
                        echo $bodytag;
                        @endphp
                        @endif
                        @if($product->category_id == 2)
                        <p class="card-text"> Group Items :
                            @foreach ($product_lists as $ln)
                        <ul>
                            @if(in_array($ln->id, json_decode($product->group_items, true)))
                            {{ $loop->first ? '' : ', ' }}
                            <li>
                                <a href="{{ route('product_detail', $ln->id) }}" title="View Detail: {{ $ln->name }}"> {{ $ln->name }}</a>
                            </li>
                            @endif
                        </ul>
                        @endforeach

                        </p>
                        @endif
                        @if(isset($product->tags))

                        <p class="card-text"> Tags : {{ $product->tags }}</p>

                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</div>
</div>

@endsection