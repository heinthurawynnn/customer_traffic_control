<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BranchController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DeptsController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\MercatController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductCodeController;
use App\Http\Controllers\ProductDesignController;
use App\Http\Controllers\ProductGroupController;
use App\Http\Controllers\ProductPatternController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\FieldController;
use App\Http\Controllers\PeriodController;
use App\Http\Controllers\PropertieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [HomeController::class, 'home'])->name('home');
Route::get('/welcome', [HomeController::class, 'welcome'])->name('welcome');
Route::get('/create', [HomeController::class, 'create'])->name('create');
Route::get('/documents_list', [HomeController::class, 'document_list'])->name('documents_list');

Route::get('/doc_create', [HomeController::class, 'prepare'])->name('doc_create');
Route::get('/new_code_form', [HomeController::class, 'new_code_form'])->name('new_code_form');

Route::get('/user_register', [EmployeeController::class, 'register'])->name('user_register');
Route::get('/user_login', [EmployeeController::class, 'login'])->name('user_login');
Route::post('/check_login', [EmployeeController::class, 'check_login'])->name('check_login');

Route::get('/user_reset_pwd', [EmployeeController::class, 'user_reset_pwd'])->name('user_reset_pwd');
Route::post('/reset_pwd', [EmployeeController::class, 'reset_pwd'])->name('reset_pwd');

Route::get('/otp_input', [EmployeeController::class, 'otp_input'])->name('otp_input');
Route::post('/otp_check', [EmployeeController::class, 'otp_check'])->name('otp_check');
Route::resource('employee', EmployeeController::class);
Route::get('dept/{id}', [EmployeeController::class, 'getBranch'])->name('get_dept');
Route::get('documents/export/', [DocumentController::class, 'export']);

Route::get('test', [HomeController::class, 'test'])->name('test');




Auth::routes();


Route::group(['middleware' => ['auth']], function () {

    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('users', UserController::class);
    Route::resource('brands', BrandController::class);
    Route::resource('branches', BranchController::class);
    Route::resource('suppliers', SupplierController::class);
    Route::resource('depts', DeptsController::class);
    Route::resource('units', UnitController::class);
    Route::resource('mercats', MercatController::class);
    Route::resource('product_Categories', ProductCategoryController::class);
    Route::resource('product_Groups', ProductGroupController::class);
    Route::resource('product_Patterns', ProductPatternController::class);
    Route::resource('product_Designs', ProductDesignController::class);
    Route::resource('product_Codes', ProductCodeController::class);
    Route::resource('documents', DocumentController::class);
    Route::resource('customers', CustomerController::class);
    Route::post('customers.search', [CustomerController::class, 'search'])->name('customers.search');
    Route::get('customers.search', [CustomerController::class, 'search'])->name('customers.search');
    Route::resource('fields', FieldController::class);
    Route::resource('periods', PeriodController::class);
    Route::resource('properties', PropertieController::class);




    // Data table doc list
    Route::get('/document_list', [HomeController::class, 'document_list'])->name('document_list');

    Route::get('change_password', [EmployeeController::class, 'change_pwd'])->name('change_pwd');
    Route::patch('update_pwd/{users}', [EmployeeController::class, 'update_pwd'])->name('users.update_pwd');

    // Ajax Category Group Pattern Design

    Route::get('/pcode_generate', [ProductCodeController::class, 'get_pcode_gen'])->name('pcode_generate');
    Route::get('product_Codes/cat/{id}', [ProductCodeController::class, 'get_groups'])->name('get_p_group');
    Route::get('product_Codes/gp/{id}', [ProductCodeController::class, 'get_patterns'])->name('get_p_pattern');
    Route::get('product_Codes/ptn/{id}', [ProductCodeController::class, 'get_designs'])->name('get_p_desgin');

    Route::get('product_category/{id}', [ProductCodeController::class, 'get_category_by_id'])->name('category_name_by_id');

    Route::get('users/branch/{id}', [UserController::class, 'getdept'])->name('get_p_group');
    Route::get('users/create/{id}', [UserController::class, 'getdept'])->name('get_p_group');

    // Route::get('/user_register', [EmployeeController::class, 'register'])->name('user_register');
    // Route::get('branches_search', [BranchController::class, 'search'])->name('branches.search');
    Route::get('/admin', [HomeController::class, 'admin'])->name('admin');

    Route::get('/prepare', [HomeController::class, 'prepare'])->name('prepare');
    Route::post('doc_store/{id}', [HomeController::class, 'doc_prepare_save'])->name('doc_store');
    Route::get('prepare_update/{id}', [HomeController::class, 'prepare_update'])->name('prepare_update');
    Route::post('pcode_store', [HomeController::class, 'pcode_store'])->name('pcode_store');
    Route::get('manager_product_codes_edit/{id}', [HomeController::class, 'manager_product_codes_edit'])->name('manager_product_codes_edit');
    Route::post('manager_product_codes_update/{id}', [HomeController::class, 'manager_product_codes_update'])->name('manager_product_codes_update');

    Route::delete('pcode_destory/{id}', [HomeController::class, 'pcode_destory'])->name('pcode_destory');
    Route::get('/approve', [HomeController::class, 'approve'])->name('approve');
    Route::get('/export', [HomeController::class, 'export'])->name('export');
    Route::get('document_view/{id}', [HomeController::class, 'document_view'])->name('document_view');
    Route::get('approve_view/{id}', [HomeController::class, 'approve_view'])->name('approve_view');
    Route::post('approve_store/{id}', [HomeController::class, 'doc_approve_save'])->name('approve_store');
    Route::post('checked/{id}', [HomeController::class, 'doc_checked_save'])->name('checked');
    Route::delete('doc_destory/{id}', [HomeController::class, 'doc_destory'])->name('doc_destory');
    Route::get('export_script/{id}', [HomeController::class, 'export_script'])->name('export_script');
    Route::post('doc_export/{id}', [HomeController::class, 'doc_export'])->name('doc_export');
    Route::get('/approve/list', [HomeController::class, 'manager_list'])->name('document_list_for_manager');
    Route::get('/export/list', [HomeController::class, 'sd_list'])->name('document_list_for_SD');

    Route::get('/check/list', [HomeController::class, 'checker_list'])->name('document_list_for_checker');

    // Route::get('/prepare_update', [HomeController::class, 'index1'])->name('index1');
    Route::post('/autocomplete/fetch', [HomeController::class, 'fetch'])->name('autocomplete.fetch');
    Route::post('/autocomplete/vendorfetch', [HomeController::class, 'vendorfetch'])->name('autocomplete.vendorfetch');
    Route::post('/autocomplete/brandfetch', [HomeController::class, 'brandfetch'])->name('autocomplete.brandfetch');


    Route::get('/checker', [HomeController::class, 'checker'])->name('checker');
});
